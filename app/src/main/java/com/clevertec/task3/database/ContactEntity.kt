package com.clevertec.task3.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contacts")
data class ContactEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    var number: Int,
    var name: String,
    var surname: String? = null,
    var email: String? = null
){

}