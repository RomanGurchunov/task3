package com.clevertec.task3.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface ContactDao {

    @Insert
    fun insert(contact: ContactDao)

}