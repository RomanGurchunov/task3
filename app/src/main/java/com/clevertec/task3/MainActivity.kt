package com.clevertec.task3

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.clevertec.task3.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    val REQUEST_CODE = 111
    lateinit var binding: ActivityMainBinding
    lateinit var btnSelectContact: Button
    lateinit var btnShowContact: Button
    lateinit var btnShowFromSP: Button

    val ACTION_CONTACT = "content://contacts/people"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        btnSelectContact = binding.selectContact
        btnShowContact = binding.showContact
        btnShowFromSP = binding.showFromSP

        selectContact()
    }

    private fun selectContact() {
        btnSelectContact.setOnClickListener {
            val permissionStatus =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)

            if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
                readContacts()
            } else {
                Toast.makeText(this, "Нет доступа", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun readContacts() {
        val intent = Intent(Intent.ACTION_PICK, Uri.parse(ACTION_CONTACT))
        startActivityForResult(intent, REQUEST_CODE)
    }
}